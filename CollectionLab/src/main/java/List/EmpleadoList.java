/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package List;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Harvey
 */
public class EmpleadoList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Empleado a = new Empleado("Harvey");
        Empleado b = new Empleado("Ana");
        Empleado c = new Empleado("Pepito");
        Empleado d = new Empleado("Lindesy");
        Empleado e = new Empleado("Yasser");
        Empleado f = new Empleado("Marlon");
        Empleado g = new Empleado("Christie");
        Empleado h = new Empleado("Gabriela");
        Empleado i = new Empleado("Eva");
        Empleado j = new Empleado("Monique");
        Empleado k = new Empleado("Alice");
        
        Set <Empleado> empleadosUni = new HashSet<Empleado>();
        
        empleadosUni.add(a);
        empleadosUni.add(b);
        empleadosUni.add(c);
        empleadosUni.add(d);
        empleadosUni.add(e);
        empleadosUni.add(f);
        empleadosUni.add(g);
        empleadosUni.add(h);
        empleadosUni.add(i);
        empleadosUni.add(j);
        empleadosUni.add(k);
        
        //AHORA IMPRIMIMOS LA LISTA
        for(Empleado listaE : empleadosUni){
            System.out.println(listaE.getEmpleado() + "  ");
        }
    }
    
}
