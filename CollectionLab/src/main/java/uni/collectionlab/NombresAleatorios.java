/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.collectionlab;

/**
 *
 * @author Harvey
 */
public class NombresAleatorios {

    public static String[] generarNombresAleatorios(int mount) {
	String[] nombresAleatorios = new String[mount];

	String[] nombres = { "Ana", "Pepito", "Lindsey", "Katy", "Maloon", "Jakie", "Robert", "Pedro",
			"Carlos", "Caleb", "Harvey", "Yasser", "Juana", "John", "James", "Charlie", "Dom",
			"Catrina", "Karla", "Erick"};
	String[] apellidos = { "Conda", "Perez", "Fletes", "Vilchez", "Tapia", "Mendoza", "Espinoza", "Carballo",
			"Martinez", "Lopez", "Chevchenko", "Vladir", "Sososky", "Contreras", "Galeano", "Gutierres", "Demidovich",
			"Vasquez" };

	for (int i = 0; i < mount; i++) {
            nombresAleatorios[i] = nombres[(int) (Math.floor(Math.random() * ((nombres.length - 1) - 0 + 1) + 0))] + " "
			+ apellidos[(int) (Math.floor(Math.random() * ((apellidos.length - 1) - 0 + 1) + 0))];
		}
            return nombresAleatorios;
	}


    public static void imprimir(String[] nombresGenerados) {
	for (int i = 0; i < nombresGenerados.length; i++) {
            System.out.println(nombresGenerados[i]);
            }
	}

    public static void main(String[] args) {
        System.out.println("====================================================================");
        System.out.println("El Ganador del Sorteo de Una Beca de Estudio Patrocinado por la ");
        System.out.println("Universidad Nacional de Ingenieria es para: ");
        imprimir(generarNombresAleatorios(1));
        System.out.println("====================================================================");
    }    
}
