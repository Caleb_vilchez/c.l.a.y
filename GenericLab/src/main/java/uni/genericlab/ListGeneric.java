/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.genericlab;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Harvey
 */
public class ListGeneric {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        List<String> generic = new ArrayList<>();
        generic.add("Janahaira Patricia");
        generic.add("Janixa Benavides");
        generic.add("Angela Soto");
        generic.add("Centeno Thompson");
        generic.add("Cruz Chill");
        Ordenar(generic);
        System.out.println(generic);
    }

    public static <T extends Comparable<T>> void Ordenar(List<T> namae){
        T aux;
        for (int i = 1; i < namae.size(); i++) {
            aux = namae.get(i);
            int j = i - 1;
            while ((j >= 0) && (aux.compareTo(namae.get(j))) < 0) {
                namae.remove(j + 1);
                namae.add(j + 1, namae.get(j));
                j--;
            }
            namae.remove(j + 1);
            namae.add(j + 1, aux);
        }

    }
    
}
