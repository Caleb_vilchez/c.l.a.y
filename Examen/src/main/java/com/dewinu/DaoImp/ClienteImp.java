/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dewinu.DaoImp;

import com.dewinu.Dao.ClienteModel;
import com.dewinu.Pojo.Cliente;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sistemas36
 */
public class ClienteImp implements ClienteModel{
    private File fHead;
    private File fData;
    private RandomAccessFile rafHead;
    private RandomAccessFile rafData;
    private final int SIZE = 750; //le pongo mas para no cuadrear.
    private Gson gson;

    public ClienteImp() {
        gson = new Gson();
    }
    
    private void open() throws FileNotFoundException, IOException{
        fHead = new File("clientes.head");
        fData = new File("clientes.data");
        
        rafHead = new RandomAccessFile(fHead, "rw");
        rafData = new RandomAccessFile(fData, "rw");
        
        if(rafHead.length() <= 0){
            rafHead.seek(0);
            rafHead.writeInt(0);
            rafHead.writeInt(0);
        }
        
    }
    
    private void close() throws IOException{
        if(rafHead != null){
            rafHead.close();
        }
        if(rafData != null){
            rafData.close();
        }
    }
    
    @Override
    public void create(Cliente t) throws IOException {
        open();
        rafHead.seek(0);
        int n = rafHead.readInt(); //Creamos N
        int k = rafHead.readInt(); //Creamos K
        
        long posData = k * SIZE; //Posicion de los datos actualmente
        
        t.setId(k+1); //Sincroniza con el ID que tenemos en nuestro objeto
        String jsonCliente = gson.toJson(t); //Convierte el objeto en un JSON y lo guarda en el string jsonCliente
        
        
        rafData.seek(posData); //Nos vamos a la poscion de la DATA
        rafData.writeUTF(jsonCliente); //Escribe el registro
        
        //Actulizamos HEAD
        long posHead = 8 + 4 * n; //Calculamos la posicion de
        
        rafHead.seek(0); //Nos vamos a las posicion 0 para actualiSar n y k
        rafHead.writeInt(++n); //Actulizamos N
        rafHead.writeInt(++k); //Aculizamos K
        
        rafHead.seek(posHead); //Nos vamos a la posición anteriormente calculada
        rafHead.writeInt(k); //Se escribe el primero ID ó INDEX
        close();
    }
    
    @Override
    public void update(Cliente e, int id) throws IOException {
        open(); //Abrimos el archivos
        long posHead = 8 + 4 * (id - 1); //CALCULAMOS la posición de la cabecera que nos apunta al archivo que queremos actulizar.
        rafHead.seek(posHead); //NOS dirigimos a la posicion de la cabecera calculada anteriormente
        long posData = (id - 1) * SIZE; //Calculamos la posicion de la DATA que queremos actulizar
        e.setId(id); //Le asignamos el ID al objeto.
        rafData.seek(posData); //Nos vamos  a la posicion de DATA
        String jsonEmpleado = gson.toJson(e); //Convertimos nuestro objeto a JSON y este lo guardamos en un string
        rafData.writeUTF(jsonEmpleado); //Escribimos ese string
        close(); //SE CIERRRA
    }

    @Override
    public List<Cliente> showAll() throws IOException {
        open(); //Abrimos el archivo
        List<Cliente> clientes = new ArrayList<>(); //Declaramos la lista que guardará los registros a mostrar 
        rafHead.seek(0); //Nos dirigimos al inicio de la cabecera
        int n = rafHead.readInt();//Leemos el total de registros.

        for (int i = 0; i < n; i++) { //Un for de toda la vida
            long posHead = 8 + 4 * i; // Calculamos la posición de la cabecera
            rafHead.seek(posHead); //Nos vamos a esa posicion

            int index = rafHead.readInt(); //Leemos el INDEX
            long posData = (index - 1) /*Porque si*/ * SIZE; //Calculamos la posicion de la DATA a leer
            rafData.seek(posData); // Nos vamos a esa posicion

            String jsonCliente = rafData.readUTF(); //Leemos lo que está alli y se guarda en un string 
            Cliente t = gson.fromJson(jsonCliente, Cliente.class); //Ese string se transforma a un objeto cliente
            clientes.add(t); //Se añade ese objeto a la lista.
        }

        close();//SE CIERRA
        return clientes;
    }

    @Override
    public Cliente showById(int id) throws IOException {
        open(); //Abrimos el archivo
        rafHead.seek(0); //Vamos al inicio de la cabecera
        int n = rafHead.readInt(); //Leemos N
        int k = rafHead.readInt(); //Leemos K
        
        if(id >  n  || id <= 0){ //Verificamos que el ID Exista
            return null;
        }
        
        int index = IOCollections.binarySearchInt(rafHead, id, 0, n-1); //Buscamos la posición del ID
        long posHead = 8 + 4 * index; //Calculamos la posicion de la cabecera
        rafHead.seek(posHead); //Nos Vamos a la posición de la cabecera
        int code = rafHead.readInt(); //Leemos el ID que apunta al objeto que uqeremos mostrar- 
        
        long posData =(id - 1) * SIZE; //Calculamos la posicion de la Data que queremos leer.
        rafData.seek(posData); //Nos vamos  la posicion de la DATA
        
        String jsonEmpleado = rafData.readUTF(); //Leer el registro y guardarlo en un string
        Cliente e = gson.fromJson(jsonEmpleado, Cliente.class); //Convierte el string a un objeto cliente
        
        if(code != id){ //Si el codigo es distinto del ID, entonces algo salio mal, retonará null
            close();
            return null;
        }
        else{
            close(); //Si el codigo == ID entonces delvoverá el empleado.
            return e;
        }
        
    }

    @Override
    public List<Cliente> showByName(String name) throws IOException {
        List<Cliente> empleados = showAll();
        List<Cliente> empleadosName = new ArrayList<>();
        
        for(Cliente e: empleados){
           if(e.equalsName(name)){
               empleadosName.add(e);
           }
        }
        if(empleadosName == null){
            return null;
        }
        else{
            return empleadosName;
        }
    }

    @Override
    public List<Cliente> showByLastName(String lastName) throws IOException {
        List<Cliente> empleados = showAll();
        List<Cliente> empleadosLastName = new ArrayList<>();
        
        for(Cliente e: empleados){
            if(e.equalsApellido(lastName)){
                empleadosLastName.add(e);
            }
        }
        if(empleadosLastName == null){
            return null;
        }
        else{
            return empleadosLastName;
        }
    }

    

    
    
}
