/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dewinu.DaoImp;

import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author Dewin U
 */
public class IOCollections {
    public static int binarySearchInt(RandomAccessFile raf, int key, int low, int high) throws IOException {
        int index = -1;

        while (low <= high) {
            int mid = (low + high) / 2;
            long pos = 8 + 4*mid;
            raf.seek(pos);
            int id = raf.readInt();
            if (id < key) {
                low = mid + 1;
            } else if (id > key) {
                high = mid - 1;
            } else if (id == key) {
                index = mid;
                break;
            }
        }
        return index;
    }
    
}
