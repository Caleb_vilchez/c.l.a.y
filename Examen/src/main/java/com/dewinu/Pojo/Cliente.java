/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dewinu.Pojo;

/**
 *
 * @author Sistemas36
 */  
public class Cliente {
    private int id; // 4
    private String nombres; //80
    private String apellidos; //80
    private String nombreEmpresa; //100
    private int tipoPersona; //4
    private String telefono; //40
    private String cedula; // 80
    private String correo; //120
    private String direccion; //200

    public Cliente() {
    }
    
    

    public Cliente(String nombres, String apellidos, String nombreEmpresa, int tipoPersona, String telefono, String cedula, String correo, String dirrecion) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.nombreEmpresa = nombreEmpresa;
        this.tipoPersona = tipoPersona;
        this.telefono = telefono;
        this.cedula = cedula;
        this.correo = correo;
        this.direccion = dirrecion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public int getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(int tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String dirrecion) {
        this.direccion = dirrecion;
    }
    
    
    public boolean equalsName(String name) {
        if (this.nombres.compareToIgnoreCase(name) != 0){
            return false;
        }
        else
            return true;
    }
    
    public boolean equalsApellido(String lastName) {
        if(this.apellidos.compareToIgnoreCase(lastName) != 0){
            return false;
        }
        else{
            return true;
        }
    }
        
    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", nombres=" + nombres + ", apellidos=" + apellidos + ", nombreEmpresa=" + nombreEmpresa + ", tipoPersona=" + tipoPersona + ", telefono=" + telefono + ", cedula=" + cedula + ", correo=" + correo + ", direccion=" + direccion + '}';
    }

    
    
    
    
    
}
