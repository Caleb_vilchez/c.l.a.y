/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dewinu.Dao;

import com.dewinu.Pojo.Cliente;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Sistemas36
 */
public interface ClienteModel extends Model<Cliente>{
    Cliente showById(int code) throws IOException;
    List<Cliente> showByName(String name) throws IOException;
    List<Cliente> showByLastName(String lastName) throws IOException;
}
