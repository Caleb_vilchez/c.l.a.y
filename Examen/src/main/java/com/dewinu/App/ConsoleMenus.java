/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dewinu.App;

import com.dewinu.DaoImp.ClienteImp;
import com.dewinu.Pojo.Cliente;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Sistemas36
 */
public class ConsoleMenus {
    Scanner teclado = new Scanner(System.in);
    String s;

    public void MainMenu() throws IOException
    {
        ClienteImp archivo = new ClienteImp();
        
        System.out.println("Bienvenido! - Gestor de Clientes v0.01");
        System.out.println("Seleccione una opcion:");
        System.out.println("1. Registar Cliente");
        System.out.println("2. Visualizar Todos");
        System.out.println("3. Visualizar por id");
        System.out.println("4. Salir");
        System.out.print("\n\tTu Opcion: ");
        s = teclado.next();
        
        try{
            int opc = Integer.parseInt(s.trim());
            switch(opc){
                case 1:
                    int tipo;
                    String name, lastName, cedula, empresa, phone, email,address;
                    System.out.print("Nombre: ");
                    name = teclado.next();
                    System.out.print("\nApellido: ");
                    lastName = teclado.next();
                    System.out.print("\nCedula: ");
                    cedula =  teclado.next();
                    System.out.print("\nNombre De la empresa: ");
                    empresa = teclado.next();
                    System.out.print("\nTipó de pérsona (0:Judirica, 1:Natural): ");
                    tipo = teclado.nextInt();
                    System.out.print("\nTelefono: ");
                    phone = teclado.next();
                    System.out.print("\nCorreo: ");
                    email = teclado.next();
                    System.out.print("\nDirecion: ");
                    address = teclado.next();
                    Cliente c = new Cliente(name,lastName,empresa,tipo,phone,cedula,email,address);
                    archivo.create(c);
                    System.out.println("Registro Creado!");
                    System.out.println("Presione ENTER para continuar");
                    System.in.read();
                    break;
                case 2:
                    List<Cliente> clientes = archivo.showAll();
                    for(Cliente t : clientes){
                        System.out.println(t.toString());
                    }
                    System.out.println("Presione ENTER para continuar");
                    System.in.read();
                    break;
                case 3:
                    System.out.print("Ingresa el id a buscar: ");
                    int code = teclado.nextInt();
                    Cliente t = archivo.showById(code);
                    if(t == null){
                        System.out.print("No se encontro chele!");
                        System.out.println("Presione ENTER para continuar");
                        System.in.read();
                    }
                    else{
                        System.out.println(t.toString());
                        System.out.println("Presione ENTER para continuar");
                        System.in.read();
                    }
                    break;
                    
                case 4:
                    System.out.println("Hasta Pronto :)");
                    System.out.println("Presione ENTER para continuar");
                    System.in.read();
            }
        }
        catch(NumberFormatException nfe){
            System.out.println("Input Incorrecto");
            System.out.println("Presione ENTER para continuar");
            System.in.read();
            MainMenu();
        }    
    }
}
    
    
