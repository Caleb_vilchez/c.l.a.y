/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author Harvey
 */
public class Empleado implements Serializable{
    private String codigo;          //43
    private String nombres;         //43
    private String cargo;           //43
    private String departamento;    //43
    private int extras;             //4
    private int antiguedad;         //4
    private int bonificacion;       //4
    private int deducciones;        //4
    private int sueldo;             //4
    boolean activo;                 //1

    public Empleado() {
        codigo = "CC";
        nombres = "NN";
        cargo = "CR";
        departamento = "DP";
        extras = 0;
        antiguedad = 0;
        bonificacion = 0;
        deducciones = 0;
        sueldo = 0;
        activo = true;
    }

    public Empleado(String codigo, String nombres, String cargo, String departamento, int extras, int antiguedad, int bonificacion, int deducciones, int sueldo, boolean activo) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.cargo = cargo;
        this.departamento = departamento;
        this.extras = extras;
        this.antiguedad = antiguedad;
        this.bonificacion = bonificacion;
        this.deducciones = deducciones;
        this.sueldo = sueldo;
        this.activo = activo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public int getExtras() {
        return extras;
    }

    public void setExtras(int extras) {
        this.extras = extras;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public int getBonificacion() {
        return bonificacion;
    }

    public void setBonificacion(int bonificacion) {
        this.bonificacion = bonificacion;
    }

    public int getDeducciones() {
        return deducciones;
    }

    public void setDeducciones(int deducciones) {
        this.deducciones = deducciones;
    }

    public int getSueldo() {
        return sueldo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Empleado{" + "codigo=" + codigo + ", nombres=" + nombres 
                + ", cargo=" + cargo + ", departamento=" + departamento 
                + ", extras=" + extras + ", antiguedad=" + antiguedad 
                + ", bonificacion=" + bonificacion + ", deducciones=" 
                + deducciones + ", sueldo=" + sueldo + '}';
    }

    public int getTamano(){
        return getCodigo().length()*2+2+43+43+43+4+4+4+4+4+1;
    }
    
}
