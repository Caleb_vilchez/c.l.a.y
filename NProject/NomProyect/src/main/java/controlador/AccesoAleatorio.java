/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import modelo.Empleado;

/**
 *
 * @author Harvey
 */
public class AccesoAleatorio {
    private static RandomAccessFile flujo;
    private static int numeroRegistros;
    private static int tamanoRegistro = 196;
    
    public static void crearFileEmpleado(File archivo) throws IOException {
        if (archivo.exists() && !archivo.isFile()) {
            throw new IOException(archivo.getName() + " No es un archivo");
        }
        flujo = new RandomAccessFile(archivo, "rw");
        numeroRegistros = (int) Math.ceil(
                (double) flujo.length() / (double) tamanoRegistro);
    }
    
    public static void cerrar () throws IOException {
        flujo.close();
    }
    
    public static boolean setEmpleado(int i, Empleado empleado) throws IOException {
        if(i >= 0 && i <= getNumeroRegistros()) {
            if(empleado.getTamano() > tamanoRegistro) {
                System.out.println("\nTamaño de registro excedido.");
            } else {
                flujo.seek(i*tamanoRegistro);
                flujo.writeUTF(empleado.getCodigo());
                flujo.writeUTF(empleado.getNombres());
                flujo.writeUTF(empleado.getCargo());
                flujo.writeUTF(empleado.getDepartamento());
                flujo.writeInt(empleado.getExtras());
                flujo.writeInt(empleado.getAntiguedad());
                flujo.writeInt(empleado.getBonificacion());
                flujo.writeInt(empleado.getDeducciones());
                flujo.writeInt(empleado.getSueldo());
                flujo.writeBoolean(empleado.isActivo());
                return true;
            }
        } else {
            System.out.println("\nNúmero de registro fuera de límites.");
        }
        return false;
    }
    
    private static int buscarRegistroInactivo() throws IOException {
        String nombres;
        for(int i=0; i<getNumeroRegistros(); i++) 
        {
            flujo.seek(i * tamanoRegistro);
            if(!getEmpleado(i).isActivo()) 
                return i;
        }
        return -1;        
    }
    
    public static boolean eliminarEmpleado(String aEliminar) throws IOException {
        int pos = buscarRegistro(aEliminar);
        if(pos == -1) return false;
        Empleado empleadoEliminado = getEmpleado(pos);
        empleadoEliminado.setActivo(false);
        setEmpleado(pos, empleadoEliminado);
        return true;
    }
    
    public static void compactarArchivo(File archivo) throws IOException {
        crearFileEmpleado(archivo); // Abrimos el flujo.
        Empleado[] listado = new Empleado[numeroRegistros];
        for(int i=0; i<numeroRegistros; ++i)
            listado[i] = getEmpleado(i);
        cerrar(); // Cerramos el flujo.
        archivo.delete(); // Borramos el archivo.

        File tempo = new File("temporal.dat");
        crearFileEmpleado(tempo); // Como no existe se crea.
        for(Empleado p : listado)
            if(p.isActivo())
                anadirEmpleado(p);
        cerrar();
        
        tempo.renameTo(archivo); // Renombramos.
    }
    
    public static void anadirEmpleado(Empleado empleado) throws IOException {
        int inactivo = buscarRegistroInactivo();
        if(setEmpleado(inactivo==-1?numeroRegistros:inactivo, empleado)) 
            numeroRegistros++;        
    }
    
    public static int getNumeroRegistros() {
        return numeroRegistros;
    }
    
    public static Empleado getEmpleado(int i) throws IOException {
        if(i >= 0 && i <= getNumeroRegistros()) {
            flujo.seek(i * tamanoRegistro);
            return new Empleado(flujo.readUTF(), flujo.readUTF(), flujo.readUTF(),
                    flujo.readUTF(), flujo.readInt(),flujo.readInt(), flujo.readInt(), flujo.readInt(),
                    flujo.readInt(), flujo.readBoolean());
        } else {
            System.out.println("\nNúmero de registro fuera de límites.");
            return null;
        }
    }
    
    public static int buscarRegistro(String buscado) throws IOException {
        Empleado p;
        if (buscado == null) {
            return -1;
        }
        for(int i=0; i<getNumeroRegistros(); i++) {
            flujo.seek(i * tamanoRegistro);
            p = getEmpleado(i);
            if(p.getNombres().equals(buscado) && p.isActivo()) {
                return i;
            }
        }
        return -1;
    }
}
