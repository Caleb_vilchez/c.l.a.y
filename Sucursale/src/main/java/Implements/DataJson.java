/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Implements;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import pojo.Sucursal;

/**
 *
 * @author Harvey
 */
public class DataJson {
    
    Sucursal[] sucursales;
    public void createSucursales() throws FileNotFoundException, IOException {
        SucursalDaoImp sucursalImp = new SucursalDaoImp();
        Gson gson = new Gson();
        sucursales = gson.fromJson(new FileReader("MOCK_DATA_SUCURSALES.json"), Sucursal[].class);
        System.out.println("Se estan Cargando los Datos de las Sucursales ...\n");
        for(Sucursal s : sucursales){
            sucursalImp.create(s);
        }
        System.out.println("Los Datos de las Sucursales estan Listas\n\n");
    }
}
