/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Implements;

import dao.SucursalDao;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import pojo.Sucursal;

/**
 *
 * @author Harvey
 */
public class SucursalDaoImp implements SucursalDao{
    private File fileHeader;
    private File fileData;
    private RandomAccessFile randomFileHeader;
    private RandomAccessFile randomFileData;
    private final int SIZE= 505;
    private Sucursal s;

    public SucursalDaoImp() {
    }

    private void open() throws IOException {
        fileHeader= new File("sucursal.header");
        fileData= new File("sucursal.dat");
        if(!fileHeader.exists()) {
            fileHeader.createNewFile();
            fileData.createNewFile();
            randomFileHeader= new RandomAccessFile(fileHeader, "rw");
            randomFileData= new RandomAccessFile(fileData, "rw");
            randomFileHeader.seek(0);
            randomFileHeader.writeInt(0);//n
            randomFileHeader.writeInt(0);//k
        }else {
            randomFileHeader= new RandomAccessFile(fileHeader, "rw");
            randomFileData= new RandomAccessFile(fileData, "rw");
        }
    }
    
    private void close() throws IOException {
        if(randomFileHeader != null) {
            randomFileHeader.close();
        }
        if(randomFileData != null) {
            randomFileData.close();
            
        }
    }

    @Override
    public Sucursal findByCode(int id1) throws IOException {
        Sucursal sucursal = null;
        open();
        randomFileHeader.seek(0);
        int n= randomFileHeader.readInt();
        int k= randomFileHeader.readInt();
        
        for(int i = 0; i < n; i++) {
            long posicionHeader= 8 + (4 * i);
            randomFileHeader.seek(posicionHeader);
            int id= randomFileHeader.readInt();
            
            long posicionData= (id - 1) * SIZE;
            randomFileData.seek(posicionData);
            
            s= new Sucursal();
            s.setId(randomFileData.readInt());
            s.setNombreSucursal(randomFileData.readUTF());
            s.setRepresentante(randomFileData.readUTF());
            s.setMunicipio(randomFileData.readUTF());
            s.setDireccion(randomFileData.readUTF());
            s.setTelefono(randomFileData.readUTF());
            s.setCorreo(randomFileData.readUTF());
            s.setActivo(randomFileData.readBoolean());
            s.setFechaCreacion(randomFileData.readUTF());
            s.setFechaModificacion(randomFileData.readUTF());
            
            if (id1 == s.getId()) {
                sucursal = s;
                break;
            }
            
        }
        close();
        return sucursal;
    }

    @Override
    public List<Sucursal> findByActivo(boolean flag) throws IOException {
        open();
        List<Sucursal> sucursales= new ArrayList<>();
        randomFileHeader.seek(0);
        int n= randomFileHeader.readInt();
        int k= randomFileHeader.readInt();
        
        for(int i = 0; i < n; i++) {
            long posicionHeader= 8 + (4 * i);
            randomFileHeader.seek(posicionHeader);
            int id= randomFileHeader.readInt();
            
            long posicionData= (id - 1) * SIZE;
            randomFileData.seek(posicionData);
            
            s= new Sucursal();
            s.setId(randomFileData.readInt());
            s.setNombreSucursal(randomFileData.readUTF());
            s.setRepresentante(randomFileData.readUTF());
            s.setMunicipio(randomFileData.readUTF());
            s.setDireccion(randomFileData.readUTF());
            s.setTelefono(randomFileData.readUTF());
            s.setCorreo(randomFileData.readUTF());
            s.setActivo(randomFileData.readBoolean());
            s.setFechaCreacion(randomFileData.readUTF());
            s.setFechaModificacion(randomFileData.readUTF());
            
            if (s.isActivo()) {
                sucursales.add(s);
            }
        }
        close();
        return sucursales;
    }

    @Override
    public void create(Sucursal t) throws IOException {
        open();
        randomFileHeader.seek(0);
        int n= randomFileHeader.readInt();
        int k= randomFileHeader.readInt();
        
        long posicionData = k * SIZE;
        
        randomFileData.seek(posicionData);
        randomFileData.writeInt(k+1);
        randomFileData.writeUTF(t.getNombreSucursal());
        randomFileData.writeUTF(t.getRepresentante());
        randomFileData.writeUTF(t.getMunicipio());
        randomFileData.writeUTF(t.getDireccion());
        randomFileData.writeUTF(t.getTelefono());
        randomFileData.writeUTF(t.getCorreo());
        randomFileData.writeBoolean(t.isActivo());
        randomFileData.writeUTF(t.getFechaCreacion());
        randomFileData.writeUTF(t.getFechaModificacion());
        
        long posicionHeader= 8 + (4 * k);
        
        randomFileHeader.seek(0);
        randomFileHeader.writeInt(++n);
        randomFileHeader.writeInt(++k);
        
        randomFileHeader.seek(posicionHeader);
        randomFileHeader.writeInt(k);
        
        close();
    }

    @Override
    public List<Sucursal> showAll() throws IOException {
        open();
        List<Sucursal> sucursales= new ArrayList<>();
        randomFileHeader.seek(0);
        int n= randomFileHeader.readInt();
        int k= randomFileHeader.readInt();
        
        for(int i = 0; i < n; i++) {
            long posicionHeader= 8 + (4 * i);
            randomFileHeader.seek(posicionHeader);
            int id= randomFileHeader.readInt();
            
            long posicionData= (id - 1) * SIZE;
            randomFileData.seek(posicionData);
            
            s= new Sucursal();
            s.setId(randomFileData.readInt());
            s.setNombreSucursal(randomFileData.readUTF());
            s.setRepresentante(randomFileData.readUTF());
            s.setMunicipio(randomFileData.readUTF());
            s.setDireccion(randomFileData.readUTF());
            s.setTelefono(randomFileData.readUTF());
            s.setCorreo(randomFileData.readUTF());
            s.setActivo(randomFileData.readBoolean());
            s.setFechaCreacion(randomFileData.readUTF());
            s.setFechaModificacion(randomFileData.readUTF());
            sucursales.add(s);
        }
        close();
        return sucursales;
    }
}
