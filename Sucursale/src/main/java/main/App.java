/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Implements.DataJson;
import Implements.SucursalDaoImp;
import java.io.IOException;
import java.util.Scanner;
import pojo.Sucursal;

/**
 *
 * @author Harvey
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Scanner imput = new Scanner(System.in);
        DataJson dataJson = new DataJson();
        dataJson.createSucursales();
        int opc;
        boolean flag = true;
        SucursalDaoImp sucursalImp;
        sucursalImp = new SucursalDaoImp();
        
        do {
            menuO();
            System.out.print("Opcion: ");
            opc = imput.nextInt();
            switch (opc) {

                case 1:
                    try {
                        
                        System.out.println("Ingrese el ID:");
                        int id = imput.nextInt();
                        
                        Sucursal sucursal = sucursalImp.findByCode(id);
                        if (sucursal != null) {
                            System.out.println(sucursal);
                        } else {
                            System.out.println("No se encontro una sucursal con este id");
                        }
                    } catch (NumberFormatException ex) {
                        System.out.println("Debe ingresar un numero entero");
                    }
                    break;
                case 2:
                        sucursalImp.showAll().forEach((suc) -> {System.out.println(suc);});
                    break;
                case 3:
                        sucursalImp.findByActivo(true).forEach((suc) -> {System.out.println(suc);});
                    break;
                case 4:
                    System.exit(0);
                    break;
                default:
                    System.err.println("Opcion invalida!");
            }
        } while (flag);
        
    }
    
    public static void menuO() {
            System.out.println("\t\tMenu de Gestion de Sucursales");
        System.out.println("1. Ver sucursal por id.");
        System.out.println("2. Ver todas las sucursales.");
        System.out.println("3. Ver sucursales activas.");
        System.out.println("4. Salir.");
        }
}
