/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.nomiproject;

/**
 *
 * @author Harvey
 */
public class Empleado {
    private String codigo;
    private String nombres;
    private String cargo;
    private String departamento;
    private String extras;
    private String antiguedad;
    private String bonificacion;
    private String deducciones;
    private String sueldo;

    public Empleado() {
    }

    
    public Empleado(String codigo, String nombres, String cargo, String departamento, String extras, String antiguedad, String bonificacion, String deducciones, String sueldo) {
        this.codigo = codigo;
        this.nombres = nombres;
        this.cargo = cargo;
        this.departamento = departamento;
        this.extras = extras;
        this.antiguedad = antiguedad;
        this.bonificacion = bonificacion;
        this.deducciones = deducciones;
        this.sueldo = sueldo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(String antiguedad) {
        this.antiguedad = antiguedad;
    }

    public String getBonificacion() {
        return bonificacion;
    }

    public void setBonificacion(String bonificacion) {
        this.bonificacion = bonificacion;
    }

    public String getDeducciones() {
        return deducciones;
    }

    public void setDeducciones(String deducciones) {
        this.deducciones = deducciones;
    }

    public String getSueldo() {
        return sueldo;
    }

    public void setSueldo(String sueldo) {
        this.sueldo = sueldo;
    }
    
    
    
}
